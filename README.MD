# TASK MANAGER

# DEVELOPER INFO
**NAME**: Aleksey Pisarev

**E-MAIL**: pisarevaleks@mail.ru
# SOFTWARE
* JDK 1.8
* Windows 10
# HARDWARE
* RAM 8GB
* CPU I7
* HDD 521GB
# RUN PROGRAM
```shell
java -jar ./task-manager.jar
```
# SCREENSHOTS
https://drive.google.com/drive/folders/15wE7sC4P7hHltCsolL_dcv4T3OEc9OGb?usp=sharing